import React from 'react';
import { hot } from 'react-hot-loader'
import { BrowserRouter as Router,Route } from 'react-router-dom';

import AppLayout from './../../layout/default';
import { RoutedContent } from '../../routes';

const basePath = process.env.BASE_PATH || '/';

const AppClient = () => {
    return (
        <Router basename={ basePath }>
            
            {/* <Route to='/' component={<div>saurav</div>}/>
            <Route to='/admin'  component={
            }/> */}
                <AppLayout>
                <RoutedContent />
                </AppLayout>
        </Router>
    );
}

export default hot(module)(AppClient);