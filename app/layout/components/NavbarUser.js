import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { authLogin,logout } from '../../store/actions/auth.js';
import { compose } from 'redux';
import {useSelector,useDispatch} from 'react-redux';
import {
    NavItem,
    NavLink
} from './../../components';

const NavbarUser = (props) => {
    // const state=useSelector(state=>state)
    const dispatch=useDispatch()
    // console.log(state)
    return(
    <NavItem { ...props }>
        <NavLink tag={ Link }  to={`${'/admin/pages/login'}`}
        
        >
            <i onClick={()=>dispatch(logout())} className="fa fa-power-off"></i>
        </NavLink>
    </NavItem>
)};
NavbarUser.propTypes = {
    className: PropTypes.string,
    style: PropTypes.object
};
// const mapStateToProps = state => {
//     return {
//       token: state.auth.token,
//       loading: state.auth.loading,
//       error: state.auth.error,
//       successAuth: state.auth.successAuth
//     };
//   };
// const mapDispatchToProps = dispatch => {
//     return {
//       onAuthLogin: (username, password) =>
//         dispatch(authLogin(username, password)),
//         logout:()=>dispatch(logout())
//     };
//   };
  


// export default connect(mapStateToProps, mapDispatchToProps)(NavbarUser);

  export {NavbarUser};