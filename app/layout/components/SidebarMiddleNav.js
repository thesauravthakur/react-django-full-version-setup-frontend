import React from 'react';

import { SidebarMenu } from './../../components';

export const SidebarMiddleNav = () => (
    <SidebarMenu>
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-home"></i>}
            title="Dashboards"
        >
            <SidebarMenu.Item title="Analytics" to='/admin/dashboards/analytics' exact />
            <SidebarMenu.Item title="Projects" to='/admin/dashboards/projects' exact />
            <SidebarMenu.Item title="System" to='/admin/dashboards/system' exact />
            <SidebarMenu.Item title="Monitor" to='/admin/dashboards/monitor' exact />
            <SidebarMenu.Item title="Financial" to='/admin/dashboards/financial' exact />
            <SidebarMenu.Item title="Stock" to='/admin/dashboards/stock' exact />
            <SidebarMenu.Item title="Reports" to='/admin/dashboards/reports' exact />
        </SidebarMenu.Item>
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-th"></i>}
            title="Widgets"
            to='/admin/widgets'
        />
        { /* -------- Cards ---------*/ }
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-clone"></i>}
            title="Cards"
        >
            <SidebarMenu.Item title="Cards" to='/admin/cards/cards' exact />
            <SidebarMenu.Item title="Cards Headers" to='/admin/cards/cardsheaders' exact />
        </SidebarMenu.Item>
        { /* -------- Layouts ---------*/ }
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-columns"></i>}
            title="Layouts"
        >
            <SidebarMenu.Item title="Navbar" to='/admin/layouts/navbar' exact />
            <SidebarMenu.Item title="Sidebar" to='/admin/layouts/sidebar' exact />
            <SidebarMenu.Item title="Sidebar A" to='/admin/layouts/sidebar-a' exact />
            <SidebarMenu.Item title="Sidebar With Navbar" to="/admin/layouts/sidebar-with-navbar" exact />
            <SidebarMenu.Item title="Drag &amp; Drop" to='/admin/layouts/dnd-layout' exact />
        </SidebarMenu.Item>
        { /* -------- Interface ---------*/ }
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-toggle-on"></i>}
            title="Interface"
        >
            <SidebarMenu.Item title="Colors" to='/admin/interface/colors' />
            <SidebarMenu.Item title="Typography" to='/admin/interface/typography' />
            <SidebarMenu.Item title="Buttons" to='/admin/interface/buttons' />
            <SidebarMenu.Item title="Paginations" to='/admin/interface/paginations' />
            <SidebarMenu.Item title="Images" to='/admin/interface/images' />
            <SidebarMenu.Item title="Avatars" to='/admin/interface/avatars' />
            <SidebarMenu.Item title="Progress Bars" to='/admin/interface/progress-bars' />
            <SidebarMenu.Item title="Badges &amp; Labels" to='/admin/interface/badges-and-labels' />
            <SidebarMenu.Item title="Media Objects" to='/admin/interface/media-objects' />
            <SidebarMenu.Item title="List Groups" to='/admin/interface/list-groups' />
            <SidebarMenu.Item title="Alerts" to='/admin/interface/alerts' />
            <SidebarMenu.Item title="Accordions" to='/admin/interface/accordions' />
            <SidebarMenu.Item title="Tabs Pills" to='/admin/interface/tabs-pills' />
            <SidebarMenu.Item title="Tooltips &amp; Popovers" to='/admin/interface/tooltips-and-popovers' />
            <SidebarMenu.Item title="Dropdowns" to='/admin/interface/dropdowns' />
            <SidebarMenu.Item title="Modals" to='/admin/interface/modals' />
            <SidebarMenu.Item title="Breadcrumbs" to='/admin/interface/breadcrumbs' />
            <SidebarMenu.Item title="Navbars" to='/admin/interface/navbars' />
            <SidebarMenu.Item title="Notifications" to='/admin/interface/notifications' />
            <SidebarMenu.Item title="Crop Image" to='/admin/interface/crop-image' />
            <SidebarMenu.Item title="Drag &amp; Drop Elements" to='/admin/interface/drag-and-drop-elements' />
            <SidebarMenu.Item title="Calendar" to='/admin/interface/calendar' />
        </SidebarMenu.Item>
        { /* -------- Graphs ---------*/ }
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-pie-chart"></i>}
            title="Graphs"
        >
            <SidebarMenu.Item title="ReCharts" to='/admin/graphs/re-charts' />
        </SidebarMenu.Item>
        { /* -------- Forms ---------*/ }
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-check-square-o"></i>}
            title="Forms"
        >
            <SidebarMenu.Item title="Forms" to='/admin/forms/forms' />
            <SidebarMenu.Item title="Forms Layouts" to='/admin/forms/forms-layouts' />
            <SidebarMenu.Item title="Input Groups" to='/admin/forms/input-groups' />
            <SidebarMenu.Item title="Wizard" to='/admin/forms/wizard' />
            <SidebarMenu.Item title="Text Mask" to='/admin/forms/text-mask' />
            <SidebarMenu.Item title="Typeahead" to='/admin/forms/typeahead' />
            <SidebarMenu.Item title="Toggles" to='/admin/forms/toggles' />
            <SidebarMenu.Item title="Editor" to='/admin/forms/editor' />
            <SidebarMenu.Item title="Date Picker" to='/admin/forms/date-picker' />
            <SidebarMenu.Item title="Dropzone" to='/admin/forms/dropzone' />
            <SidebarMenu.Item title="Sliders" to='/admin/forms/sliders' />
        </SidebarMenu.Item>
        { /* -------- Tables ---------*/ }
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-trello"></i>}
            title="Tables"
        >
            <SidebarMenu.Item title="Tables" to='/admin/tables/tables' />
            <SidebarMenu.Item title="Extended Tables" to='/admin/tables/extended-table' />
            <SidebarMenu.Item title="AgGrid" to='/admin/tables/ag-grid' />
        </SidebarMenu.Item>
        { /* -------- Apps ---------*/ }
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-mouse-pointer"></i>}
            title="Apps"
        >
            <SidebarMenu.Item title="Projects">
                <SidebarMenu.Item title="Projects List" to="/admin/apps/projects/list" />
                <SidebarMenu.Item title="Projects Grid" to="/admin/apps/projects/grid" />
            </SidebarMenu.Item>
            <SidebarMenu.Item title="Tasks">
                <SidebarMenu.Item title="Tasks List" to="/admin/apps/tasks/list" />
                <SidebarMenu.Item title="Tasks Grid" to="/admin/apps/tasks/grid" />
                <SidebarMenu.Item title="Tasks Kanban" to="/admin/apps/tasks-kanban" />
                <SidebarMenu.Item title="Tasks Details" to="/admin/apps/task-details" />
            </SidebarMenu.Item>
            <SidebarMenu.Item title="Files">
                <SidebarMenu.Item title="Files List" to="/admin/apps/files/list" />
                <SidebarMenu.Item title="Files Grid" to="/admin/apps/files/grid" />
            </SidebarMenu.Item>
            <SidebarMenu.Item title="Search Results">
                <SidebarMenu.Item title="Search Results" to="/admin/apps/search-results" />
                <SidebarMenu.Item title="Images Results" to="/admin/apps/images-results" />
                <SidebarMenu.Item title="Videos Results" to="/admin/apps/videos-results" />
                <SidebarMenu.Item title="Users Results" to="/admin/apps/users-results" />
            </SidebarMenu.Item>
            <SidebarMenu.Item title="Users">
                <SidebarMenu.Item title="Users List" to="/admin/apps/users/list" />
                <SidebarMenu.Item title="Users Grid" to="/admin/apps/users/grid" />
            </SidebarMenu.Item>
            <SidebarMenu.Item title="Gallery">
                <SidebarMenu.Item title="Gallery Grid" to="/admin/apps/gallery-grid" />
                <SidebarMenu.Item title="Gallery Table" to="/admin/apps/gallery-table" />
            </SidebarMenu.Item>
            <SidebarMenu.Item title="Mailbox">
                <SidebarMenu.Item title="Inbox" to="/admin/apps/inbox" />
                <SidebarMenu.Item title="New Email" to="/admin/apps/new-email" />
                <SidebarMenu.Item title="Email Details" to="/admin/apps/email-details" />
            </SidebarMenu.Item>
            <SidebarMenu.Item title="Profile">
                <SidebarMenu.Item title="Profile Details" to="/admin/apps/profile-details" />
                <SidebarMenu.Item title="Profile Edit" to="/admin/apps/profile-edit" />
                <SidebarMenu.Item title="Account Edit" to="/admin/apps/account-edit" />
                <SidebarMenu.Item title="Billing Edit" to="/admin/apps/billing-edit" />
                <SidebarMenu.Item title="Settings Edit" to="/admin/apps/settings-edit" />
                <SidebarMenu.Item title="Sessions Edit" to="/admin/apps/sessions-edit" />
            </SidebarMenu.Item>
            <SidebarMenu.Item title="Clients" to="/admin/apps/clients" exact />
            <SidebarMenu.Item title="Chat" to="/admin/apps/chat" exact />
        </SidebarMenu.Item>
        { /* -------- Pages ---------*/ }
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-copy"></i>}
            title="Pages"
        >
            <SidebarMenu.Item title="Register" to="/admin/pages/register" />
            <SidebarMenu.Item title="Login" to="/admin/pages/login" />
            <SidebarMenu.Item title="Forgot Password" to="/admin/pages/forgot-password" />
            <SidebarMenu.Item title="Lock Screen" to="/admin/pages/lock-screen" />
            <SidebarMenu.Item title="Error 404" to="/admin/pages/error-404" />
            <SidebarMenu.Item title="Confirmation" to="/admin/pages/confirmation" />
            <SidebarMenu.Item title="Success" to="/admin/pages/success" />
            <SidebarMenu.Item title="Danger" to="/admin/pages/danger" />
            <SidebarMenu.Item title="Coming Soon" to="/admin/pages/coming-soon" />
            <SidebarMenu.Item title="Timeline" to="/admin/pages/timeline" />
        </SidebarMenu.Item>
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-star-o"></i>}
            title="Icons"
            to='/admin/icons'
        />
       
    </SidebarMenu >
);
