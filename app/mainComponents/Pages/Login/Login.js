import React from 'react';
import { Link } from 'react-router-dom';
import MaskedInput from 'react-text-mask'
import {
    emailMask
} from 'text-mask-addons';
import { connect } from "react-redux";
import { authLogin } from '../../../store/actions/auth.js';
import { compose } from 'redux';
import {
    withStyles,
  } from '@material-ui/core/styles';
import {
    Form,
    FormGroup,
    FormText,
    Input,
    CustomInput,
    Button,
    Label,
    EmptyLayout,
    ThemeConsumer
} from './../../../components';

import { HeaderAuth } from "../../components/Pages/HeaderAuth";
import { FooterAuth } from "../../components/Pages/FooterAuth";

const useStyles = (theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: '20ch'
      },
    },
  });

  
export const validate = (values, requiredfields) => {
    const errors = {}
    const requiredFields = requiredfields
    requiredFields.forEach(field => {
      if (!values[field]) {
        errors[field] = 'Required'
      }
    })
    if (
      values.email &&
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
      errors.email = 'Invalid email address'
    }
    return errors
  }
class Login extends React.Component  {
    state = {
        username: '',
        password: '',
        redirect: null,
        showPassword: false,
        hasErrorMessage: false,
        isErrorDialogPopUp: false,
        ErrorMessage: '',
        invalidEmail:false,
      }
      render(){
        const handelSubmit = (e) => {
            e.preventDefault();
            const { username, password } = this.state
            this.props.onAuthLogin(username, password)
        }

        const { classes } = this.props;
        const { error } = this.props;
        const { successAuth } = this.props;
        console.log('success',successAuth)
        if (successAuth) {
            this.props.history.push("/admin/dashboards/projects");
        }
    return(
    <EmptyLayout>
        <EmptyLayout.Section center>
            { /* START Header */}
            <HeaderAuth 
                title="Sign In to Application"
            />
            { /* END Header */}
            { /* START Form */}
            <Form className="mb-3">
            <FormGroup>
            <Label for="email">
                Email
            </Label>
            <Input
                mask={ emailMask }
                placeholder='sauravthakur@smith.com'
                tag={ MaskedInput }
                id="email"
                onChange={(e) => { this.setState({ username: e.target.value }) }}
            />
        </FormGroup>
                <FormGroup>
                    <Label for="password">
                        Password
                    </Label>
                    <Input 
                    type="password" 
                    name="password" 
                    id="password" 
                    placeholder="Password..." 
                    className="bg-white" 
                    value={this.state.password}
                    onChange={(e) => { this.setState({ password: e.target.value }) }}
                    
                    />
                </FormGroup>
                <FormGroup>
                    <CustomInput type="checkbox" id="rememberPassword" label="Remember Password" inline />
                </FormGroup>
                <ThemeConsumer>
                {
                    ({ color }) => (
                        <Button color={ color } block tag={ Link } onClick={(e)=>handelSubmit(e)}>
                            Sign In
                        </Button>
                    )
                }
                </ThemeConsumer>
            </Form>
            { /* END Form */}
            { /* START Bottom Links */}
            <div className="d-flex mb-5">
                <Link to="/admin/pages/forgotpassword" className="text-decoration-none">
                    Forgot Password
                </Link>
                <Link to="/admin/pages/register" className="ml-auto text-decoration-none">
                    Register
                </Link>
            </div>
            { /* END Bottom Links */}
            { /* START Footer */}
            <FooterAuth />
            { /* END Footer */}
        </EmptyLayout.Section>
    </EmptyLayout>

)}}

const mapStateToProps = state => {
    return {
      token: state.auth.token,
      loading: state.auth.loading,
      error: state.auth.error,
      successAuth: state.auth.successAuth
    };
  };
const mapDispatchToProps = dispatch => {
    return {
      onAuthLogin: (username, password) =>
        dispatch(authLogin(username, password))
    };
  };
  
  export default compose(
    withStyles(useStyles, { withTheme: true }),
    connect(
      mapStateToProps,
      mapDispatchToProps
    )
  )(Login);
  