import React from 'react';
import PropTypes from 'prop-types';

const FooterText = (props) => (
	<React.Fragment>
		(C) { props.year } All Rights Reserved. This is the &quot;{ props.name }&quot; built with { props.desc }. 
		Designed and implemented by{' '}
		<a
			href="http://www.facebook.com/sauravthakurofficial"
			target="_blank"
			rel="noopener noreferrer"
			className="sidebar__link"
		>
			Saurav Thakur
		</a>
	</React.Fragment>
)
FooterText.propTypes = {
    year: PropTypes.node,
	name: PropTypes.node,
	desc: PropTypes.node,
};
FooterText.defaultProps = {
    year: "2021",
    name: "Hybrid",
    desc: "Bootstrap 4, React 16 (latest) & Yarn"
};

export { FooterText };
