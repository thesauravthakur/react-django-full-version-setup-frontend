import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { ThemeConsumer } from '../../../components/Theme';
import classes from './LogoTheamed.scss'

const logos = {
    'white': require('./../../../images/logos/logo-white.svg'),
    'primary': require('./../../../images/logos/logo-primary.svg'),
    'success': require('./../../../images/logos/logo-success.svg'),
    'warning': require('./../../../images/logos/logo-warning.svg'),
    'danger': require('./../../../images/logos/logo-danger.svg'),
    'info': require('./../../../images/logos/logo-info.svg'),
    'indigo': require('./../../../images/logos/logo-indigo.svg'),
    'purple': require('./../../../images/logos/logo-purple.svg'),
    'pink': require('./../../../images/logos/logo-pink.svg'),
    'yellow': require('./../../../images/logos/logo-yellow.svg')
}

const getLogoUrl = (style, color) => {
    return logos[color];
}

// Check for background
const getLogoUrlBackground = (style, color) => {
    if (style === 'color') {
        return logos['white'];
    } else {
        return getLogoUrl(style, color);
    }
}

const LogoThemed = ({ checkBackground, className, ...otherProps }) => (
    // <ThemeConsumer>
    // {
    //     ({ style, color }) => (
    //         <img
    //             src={
    //                 checkBackground ?
    //                     getLogoUrlBackground(style, color) :
    //                     getLogoUrl(style, color)
    //             }
    //             className={ classNames('d-block', className) }
    //             alt="Airframe Logo"
    //             { ...otherProps }
    //         />
    //     )
    // }
    // </ThemeConsumer>
    <ThemeConsumer>
    {
        ({ style, color }) => 
        
        (
            <div style={{color:`${color=='primary'?color='#1EB7FF'
            :color=='danger'?color='#ED1C24'
            :color=='success'?color='#1BB934'
            :color=='info'?color='#33AE9A'
            :color=='warning'?color='#F27212'
            :color=='indigo'?color='#6610f2'
            :color=='purple'?color='#CA8EFF'
            :color=='pink'?color='#e83e8c'
            :color=='yellow'?color='#F7BF47'
            :color=color

            }`,fontSize:'4vw',fontWeight:'bold',fontFamily:'Pattaya'}}>Hybrid</div>
        )
    }
    </ThemeConsumer>
);
LogoThemed.propTypes = {
    checkBackground: PropTypes.bool,
    className: PropTypes.string,
};

export { LogoThemed };
