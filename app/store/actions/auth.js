import axios from "axios";
import * as actionTypes from "./actionTypes";
import { mainLink } from '../utils';



export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const authSuccess = user => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    user
  };
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};

export const logout = () => {
  localStorage.removeItem("user");
  console.log('logout')
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const checkAuthTimeout = expirationTime => {
  return dispatch => {
    setTimeout(() => {
      dispatch(logout());
    }, expirationTime * 1000);
  };
};

export const updateTrainingInfo = data => {
  console.log(data)
  return {
    type: actionTypes.UPDATE_TRAINING_INFO,
    data: data
  };
};
export const clearTrainingInfo = () => {
  return {
    type: actionTypes.CLEAR_TRAINING_INFO,
  };
};


export const authLogin = (username, password,onSuccess = null, onError = null) => {
  // console.log(window.location.origin)
  // console.log(username,password)
  return dispatch => {
    dispatch(authStart());
    axios
      .post(mainLink + "/rest-auth/login/", {
        username: username.split('@')[0],
        email: username,
        password: password
      })
      .then(res => {
        
        const user = {
          token: res.data.key,
          username,
          permissions: res.data.user_type.permission,
          userId: res.data.user,
          expirationDate: new Date(new Date().getTime() + 3600 * 1000)
        };
        localStorage.setItem("user", JSON.stringify(user));
        dispatch(authSuccess(user));
        dispatch(authCheckState());
        
  
  
      })
      .catch(err => {
        dispatch(authFail(err));
        return 'saurav'
      });
  };
};

export const authSignUp = (name, address, email, password1, password2, pnumber, position, gender) => {
  return dispatch => {
    const username = email.split("@")[0]
    dispatch(authStart());
    const user = { username, email, password1, password2, address, pnumber, position, name, gender };
    console.log(user)
    axios
      .post(mainLink + "/rest-auth/registration/", user)
      .then(res => {
        const user = {
          username,
          name,
          address,
          email,
          pnumber,
          position,
          gender,
          token: res.data.key,
          userId: res.data.user,
          expirationDate: new Date(new Date().getTime() + 3600 * 1000)
        };
        console.log(user.expirationDate)
        localStorage.setItem("user", JSON.stringify(user));
        dispatch(authSuccess(user));

        dispatch(authCheckState());
        dispatch(checkAuthTimeout(3600));
      }
      )
      .catch(err => {
        console.log(err)
        dispatch(authFail(err));

      });
  };
};

export const authCheckState = () => {
  return dispatch => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user === undefined || user === null) {
      dispatch(logout());
    } else {
      const expirationDate = new Date(user.expirationDate);

      console.log(new Date(user.expirationDate), new Date())
      // if (expirationDate <= new Date()) {
      //   dispatch(logout());
      // } else {
      //   dispatch(
      //     checkAuthTimeout(
      //       (expirationDate.getTime() - new Date().getTime()) / 1000
      //     )
      //   );
      // }
    }
  };
};