import axios from "axios";
import * as actionTypes from "./actionTypes";
import { useEffect } from "react";
import { excelFormula } from "react-syntax-highlighter/dist/cjs/languages/prism";
import { mainLink } from '../utils';
import { getSetting } from './setting'

export const bookGetRackStart = () => {
  return {
    type: actionTypes.BOOK_GET_RACK_START
  };
};

export const bookGetRackSuccess = racks => {
  return {
    type: actionTypes.BOOK_GET_RACK_SUCCESS,
    racks
  };
};

export const bookGetRackFail = error => {
  return {
    type: actionTypes.BOOK_GET_RACK_FAIL,
    error: error
  };
};




export const bookRackGetStart = () => {
  return {
    type: actionTypes.BOOK_RACK_GET_START
  };
};

export const bookRackGetSuccess = racks => {
  return {
    type: actionTypes.BOOK_RACK_GET_SUCCESS,
    racks
  };
};

export const bookRackGetFail = error => {
  return {
    type: actionTypes.BOOK_RACK_GET_FAIL,
    error: error
  };
};




export const bookPostRackStart = () => {
  return {
    type: actionTypes.BOOK_POST_RACK_START
  };
};

export const bookPostRackSuccess = rack => {
  return {
    type: actionTypes.BOOK_POST_RACK_SUCCESS,
    rack
  };
};

export const bookPostRackFail = error => {
  return {
    type: actionTypes.BOOK_POST_RACK_FAIL,
    error: error
  };
};





export const bookEditRackStart = () => {
  return {
    type: actionTypes.BOOK_EDIT_RACK_START
  };
};

export const bookEditRackSuccess = rack => {
  return {
    type: actionTypes.BOOK_EDIT_RACK_SUCCESS,
    rack
  };
};

export const bookEditRackFail = error => {
  return {
    type: actionTypes.BOOK_EDIT_RACK_FAIL,
    error: error
  };
};

export const deleteBookRackFail = error => {
  return {
    type: actionTypes.BOOK_DELETE_RACK_FAIL,
    error: error
  }
}












export const bookGetRoomStart = () => {
  return {
    type: actionTypes.BOOK_ROOM_GET_START
  };
};

export const bookGetRoomSuccess = rooms => {
  return {
    type: actionTypes.BOOK_ROOM_GET_SUCCESS,
    rooms
  };
};

export const bookGetRoomFail = error => {
  return {
    type: actionTypes.BOOK_ROOM_GET_FAIL,
    error: error
  };
};

export const bookPostRoomStart = () => {
  return {
    type: actionTypes.BOOK_POST_ROOM_START
  };
};

export const bookPostRoomSuccess = room => {
  return {
    type: actionTypes.BOOK_POST_ROOM_SUCCESS,
    room
  };
};

export const bookPostRoomFail = error => {
  return {
    type: actionTypes.BOOK_POST_ROOM_FAIL,
    error: error
  };
};





export const bookEditRoomStart = () => {
  return {
    type: actionTypes.BOOK_EDIT_ROOM_START
  };
};

export const bookEditRoomSuccess = room => {
  return {
    type: actionTypes.BOOK_EDIT_ROOM_SUCCESS,
    room
  };
};

export const bookEditRoomFail = error => {
  return {
    type: actionTypes.BOOK_EDIT_ROOM_FAIL,
    error: error
  };
};

export const deleteBookRoomFail = error => {
  return {
    type: actionTypes.BOOK_DELETE_ROOM_FAIL,
    error: error
  }
}



























export const bookEntryPostStart = () => {
  return {
    type: actionTypes.BOOK_ENTRY_POST_START,
    html: null
  };
};

export const bookEntryPostSuccess = (bookEntry, printHtml = null) => {
  return {
    type: actionTypes.BOOK_ENTRY_POST_SUCCESS,
    bookEntry,
    html: printHtml
  };
};

export const bookEntryPostFail = error => {
  return {
    type: actionTypes.BOOK_ENTRY_POST_FAIL,
    error: error
  };
};






export const bookTypeGetStart = () => {
  return {
    type: actionTypes.BOOKTYPE_GET_START
  };
};

export const bookTypeGetSuccess = booktypes => {
  return {
    type: actionTypes.BOOKTYPE_GET_SUCCESS,
    booktypes
  };
};

export const bookTypeGetFail = error => {
  return {
    type: actionTypes.BOOKTYPE_GET_FAIL,
    error: error
  };
};



export const bookTypePostStart = () => {
  return {
    type: actionTypes.BOOKTYPE_POST_START
  };
};

export const bookTypePostSuccess = booktype => {
  return {
    type: actionTypes.BOOKTYPE_POST_SUCCESS,
    booktype
  };
};

export const bookTypePostFail = error => {
  return {
    type: actionTypes.BOOKTYPE_POST_FAIL,
    error: error
  };
};








export const bookTypeEditStart = () => {
  return {
    type: actionTypes.BOOKTYPE_EDIT_START
  };
};

export const bookTypeEditSuccess = booktype => {
  return {
    type: actionTypes.BOOKTYPE_EDIT_SUCCESS,
    booktype
  };
};

export const bookTypeEditFail = error => {
  return {
    type: actionTypes.BOOKTYPE_EDIT_FAIL,
    error: error
  };
};















export const bookPostStart = () => {
  return {
    type: actionTypes.BOOK_POST_START
  };
};

export const bookPostSuccess = book => {
  return {
    type: actionTypes.BOOK_POST_SUCCESS,
    book
  };
};

export const bookPostFail = error => {
  return {
    type: actionTypes.BOOK_POST_FAIL,
    error: error
  };
};








export const bookEditStart = () => {
  return {
    type: actionTypes.BOOK_EDIT_START
  };
};

export const bookEditSuccess = book => {
  return {
    type: actionTypes.BOOK_EDIT_SUCCESS,
    book
  };
};

export const bookEditFail = error => {
  return {
    type: actionTypes.BOOK_EDIT_FAIL,
    error: error
  };
};












export const bookGetStart = () => {
  return {
    type: actionTypes.BOOK_GET_START
  };
};

export const bookGetSuccess = books => {
  return {
    type: actionTypes.BOOK_GET_SUCCESS,
    books
  };
};

export const bookGetFail = error => {
  return {
    type: actionTypes.BOOK_GET_FAIL,
    error: error
  };
};



export const getBookDetailBookEntryStart = () => {
  return {
    type: actionTypes.GET_BOOK_BOOKENTRY_DETAIL_START
  }
}
export const getBookDetailBookEntrySuccess = (bookEntrys) => {
  return {
    type: actionTypes.GET_BOOK_BOOKENTRY_DETAIL_SUCCESS,
    bookEntrys
  }
}
export const getBookDetailBookEntryFail = (error) => {
  return {
    type: actionTypes.GET_BOOK_BOOKENTRY_DETAIL_FAIL,
    error
  }
}






export const getBookEntryBatchNumberStart = () => {
  return {
    type: actionTypes.GET_BOOKENTRY_BATCH_START
  }
}
export const getBookEntryBatchSuccess = (batches) => {
  return {
    type: actionTypes.GET_BOOKENTRY_BATCHES_SUCCESS,
    batches
  }
}

export const getBookEntryBatchNumberSuccess = (bookEntrysBatchResponse) => {
  return {
    type: actionTypes.GET_BOOKENTRY_BATCH_SUCCESS,
    bookEntrysBatchResponse
  }
}
export const getBookEntryBatchNumberFail = (error) => {
  return {
    type: actionTypes.GET_BOOKENTRY_BATCH_FAIL,
    error
  }
}





















export const bookPostEditionStart = () => {
  return {
    type: actionTypes.BOOK_POST_EDITION_START
  };
};

export const bookPostEditionSuccess = edition => {
  return {
    type: actionTypes.BOOK_POST_EDITION_SUCCESS,
    edition
  };
};

export const bookPostEditionFail = error => {
  return {
    type: actionTypes.BOOK_POST_EDITION_FAIL,
    error: error
  };
};



export const bookGetEditionStart = () => {
  return {
    type: actionTypes.BOOK_GET_EDITION_START
  };
};

export const bookGetEditionSuccess = editions => {
  return {
    type: actionTypes.BOOK_GET_EDITION_SUCCESS,
    editions
  };
};

export const bookGetEditionFail = error => {
  return {
    type: actionTypes.BOOK_GET_EDITION_FAIL,
    error: error
  };
};



export const bookEditEditionStart = () => {
  return {
    type: actionTypes.BOOK_EDIT_EDITION_START
  };
};

export const bookEditEditionSuccess = edition => {
  return {
    type: actionTypes.BOOK_EDIT_EDITION_SUCCESS,
    edition
  };
};

export const bookEditEditionFail = error => {

  return {
    type: actionTypes.BOOK_EDIT_EDITION_FAIL,
    error: error
  };
};








export const bookDetailEditionSuccess = detatilEdition => {
  console.log(detatilEdition)
  return {
    type: actionTypes.BOOK_DETAIL_EDITION_SUCCESS,
    detatilEdition
  };
};


















export const bookPostCategoryStart = () => {
  return {
    type: actionTypes.BOOK_POST_CATEGORY_START
  };
};

export const bookPostCategorySuccess = category => {
  return {
    type: actionTypes.BOOK_POST_CATEGORY_SUCCESS,
    category
  };
};

export const bookPostCategoryFail = error => {
  return {
    type: actionTypes.BOOK_POST_CATEGORY_FAIL,
    error: error
  };
};



export const bookGetCategoryStart = () => {
  return {
    type: actionTypes.BOOK_GET_CATEGORY_START
  };
};

export const bookGetCategorySuccess = categorys => {
  return {
    type: actionTypes.BOOK_GET_CATEGORY_SUCCESS,
    categorys
  };
};

export const bookGetCategoryFail = error => {
  return {
    type: actionTypes.BOOK_GET_CATEGORY_FAIL,
    error: error
  };
};



export const bookEditCategoryStart = () => {
  return {
    type: actionTypes.BOOK_EDIT_CATEGORY_START
  };
};

export const bookEditCategorySuccess = category => {
  return {
    type: actionTypes.BOOK_EDIT_CATEGORY_SUCCESS,
    category
  };
};

export const bookEditCategoryFail = error => {
  return {
    type: actionTypes.BOOK_EDIT_CATEGORY_FAIL,
    error: error
  };
};















export const bookPostSubCategoryStart = () => {
  return {
    type: actionTypes.BOOK_POST_SUBCATEGORY_START
  };
};

export const bookPostSubCategorySuccess = subcategory => {
  return {
    type: actionTypes.BOOK_POST_SUBCATEGORY_SUCCESS,
    subcategory
  };
};

export const bookPostSubCategoryFail = error => {
  return {
    type: actionTypes.BOOK_POST_SUBCATEGORY_FAIL,
    error: error
  };
};





export const bookGetSubCategoryStart = () => {
  return {
    type: actionTypes.BOOK_GET_SUBCATEGORY_START
  };
};

export const bookGetSubCategorySuccess = subcategorys => {
  return {
    type: actionTypes.BOOK_GET_SUBCATEGORY_SUCCESS,
    subcategorys
  };
};

export const bookGetSubCategoryFail = error => {
  return {
    type: actionTypes.BOOK_GET_SUBCATEGORY_FAIL,
    error: error
  };
};



export const bookEditSubCategoryStart = () => {
  return {
    type: actionTypes.BOOK_EDIT_SUBCATEGORY_START
  };
};

export const bookEditSubCategorySuccess = subcategory => {
  return {
    type: actionTypes.BOOK_EDIT_SUBCATEGORY_SUCCESS,
    subcategory
  };
};

export const bookEditSubCategoryFail = error => {
  return {
    type: actionTypes.BOOK_EDIT_SUBCATEGORY_FAIL,
    error: error
  };
};







export const bookGetAuthorStart = () => {
  return {
    type: actionTypes.BOOK_GET_AUTHOR_START
  };
};

export const bookGetAuthorSuccess = authors => {
  return {
    type: actionTypes.BOOK_GET_AUTHOR_SUCCESS,
    authors
  };
};

export const bookGetAuthorFail = error => {
  return {
    type: actionTypes.BOOK_GET_AUTHOR_FAIL,
    error: error
  };
};







export const bookGetPublicationStart = () => {
  return {
    type: actionTypes.BOOK_GET_PUBLICATION_START
  };
};

export const bookGetPublicationSuccess = publications => {
  return {
    type: actionTypes.BOOK_GET_PUBLICATION_SUCCESS,
    publications
  };
};

export const bookGetPublicationFail = error => {
  return {
    type: actionTypes.BOOK_GET_PUBLICATION_FAIL,
    error: error
  };
};





















export const postBookRack = (state, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostRackStart());
    const rack = {
      rack_number: state.rack_number,
      number_0f_room: state.number_0f_room,
    };
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/rack/", rack)
      .then(res => {
        dispatch(bookPostRackSuccess(rack));
        dispatch(getBookRack(token));
        dispatch(getBookRoom(token))

      }
      )
      .catch(err => {
        dispatch(bookPostRackFail(err));

      });
  };
};

export const getBookRack = (token) => {
  return dispatch => {
    dispatch(bookGetRackStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/rack/")
      .then(res => {
        console.log(res.data)
        dispatch(bookGetRackSuccess(res.data));


      }
      ).catch(err => {
        dispatch(bookGetRackFail(err));

      });
  };
};


export const editBookRack = (state, categoryId, token) => {
  return dispatch => {
    dispatch(bookEditRackStart());
    const data = {
      rack_number: state.rack_number,
      number_0f_room: state.number_0f_room
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .put(mainLink + `/api/v1/rack/${categoryId}/`, data)
      .then(res => {
        console.log(res.data)
        dispatch(bookEditRackSuccess(res.data));
        dispatch(getBookRack(token));

      }
      ).catch(err => {
        dispatch(bookEditRackFail(err));

      });
  };
};

export const deleteBookRack = (categoryId, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostRackStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .delete(mainLink + `/api/v1/rack/${categoryId}/`)
      .then(res => {
        console.log(res.data + "deleted")
        dispatch(bookPostRackSuccess(res.data));

        dispatch(getBookRack(token))
      }
      )
      .catch(err => {
        dispatch(bookPostRackFail(err));

        console.log(err)

      });
  };
};








export const postBookRoom = (state, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostRoomStart());
    const data = {
      room_number: state.room_number,
      rack_number: state.rack_number_id
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/room/", data)
      .then(res => {
        dispatch(bookPostRoomSuccess(data));
        dispatch(getBookRoom(token));

      }
      )
      .catch(err => {
        dispatch(bookPostRoomFail(err));

      });
  };
};

export const getBookRoom = (token) => {
  return dispatch => {
    dispatch(bookGetRoomStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/room/")
      .then(res => {
        console.log(res.data)
        dispatch(bookGetRoomSuccess(res.data));

      }
      ).catch(err => {
        dispatch(bookGetRoomFail(err));

      });
  };
};


export const editBookRoom = (state, categoryId, token) => {
  return dispatch => {
    const data = {
      rack_number: state.rack_number_id,
      room_number: state.room_number
    }
    dispatch(bookEditRoomStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .put(mainLink + `/api/v1/room/${categoryId}/`, data)
      .then(res => {
        console.log(res.data)
        dispatch(bookEditRoomSuccess(res.data));
        dispatch(getBookRoom(token));

      }
      ).catch(err => {
        dispatch(bookEditRoomFail(err));

      });
  };
};

export const deleteBookRoom = (categoryId, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostRoomStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .delete(mainLink + `/api/v1/room/${categoryId}/`)
      .then(res => {
        dispatch(bookGetRoomSuccess(res.data));
        dispatch(getBookRoom(token));
        console.log(res.data + "deleted")
      }
      )
      .catch(err => {
        console.log(err)
        dispatch(bookPostRoomFail(err));

      });
  };
};













export const getBookAuthor = (token) => {
  return dispatch => {
    dispatch(bookGetAuthorStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/author/")
      .then(res => {
        console.log(res.data)
        dispatch(bookGetAuthorSuccess(res.data));

      }
      ).catch(err => {
        dispatch(bookGetAuthorFail(err));

      });
  };
};









export const getBookPublication = (token) => {
  return dispatch => {
    dispatch(bookGetPublicationStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/publication/")
      .then(res => {
        console.log(res.data)
        dispatch(bookGetPublicationSuccess(res.data));

      }
      ).catch(err => {
        dispatch(bookGetPublicationFail(err));

      });
  };
};
























export const getBookType = (token) => {
  return dispatch => {
    dispatch(bookTypeGetStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/booktype/")
      .then(res => {
        dispatch(bookTypeGetSuccess(res.data));
        console.log(res.data)

      }
      ).catch(err => {
        dispatch(bookTypeGetFail(err));


      });
  };
};


export const postBookType = (bookType, token) => {

  console.log(bookType, token)
  return dispatch => {
    dispatch(bookTypePostStart());
    const data = {
      book_type_name: bookType.book_type_name,
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/booktype/", data)
      .then(res => {
        dispatch(getBookType(token))
        dispatch(bookTypePostSuccess(data));



      }
      )
      .catch(err => {
        dispatch(bookTypePostFail(err));

      });
  };
};







export const editBookType = (bookType, id, token) => {
  return dispatch => {
    dispatch(bookTypeEditStart());
    const data = {
      book_type_name: bookType.book_type_name,
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .put(mainLink + `/api/v1/booktype/${id}/`, data)
      .then(res => {
        console.log(res.data)
        dispatch(bookTypeEditSuccess(res.data));
        dispatch(getBookType(token))


      }
      ).catch(err => {
        dispatch(bookTypeEditFail(err));

      });
  };
};


export const deleteBookType = (bookId, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookTypePostStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .delete(mainLink + `/api/v1/booktype/${bookId}/`)
      .then(res => {
        dispatch(bookTypePostSuccess(bookId));
        dispatch(getBookType(token))
        console.log(res)
      }
      )
      .catch(err => {
        dispatch(bookTypePostFail(err));
        console.log(err)
      });
  };
};


































export const getBook = (token) => {
  return dispatch => {
    dispatch(bookGetStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/books/")
      .then(res => {
        console.log(res.data)
        dispatch(bookGetSuccess(res.data));
        dispatch(getBookAuthor(token))
        dispatch(getBookPublication(token))
        dispatch(getBookType(token))
        dispatch(getSetting(token))

      }
      ).catch(err => {
        dispatch(bookGetFail(err));


      });
  };
};

export const postBook = (book, token, userId) => {

  console.log(book, token, userId)
  return dispatch => {
    dispatch(bookPostStart());
    const data = {
      bookType: book.bookType,
      title: book.title,
      publication: book.publication,
      authorArrayData: book.authorArrayData,
      subcategory: book.subcategory,
      category: book.category,
      createdBy: userId,
      authorChange: book.authorChange,
      edition: book.edition,
      isbnNumber: book.isbnNumber,
      language: book.language,
      numberOfPage: book.numberOfPage,
      publishedDate: book.publishedDate
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/books/", data)
      .then(res => {
        dispatch(getBook(token))
        dispatch(bookPostSuccess(res.data));



      }
      )
      .catch(err => {
        dispatch(bookPostFail(err));

      });
  };
};



export const editBook = (book, id, token, userId) => {
  return dispatch => {
    dispatch(bookEditStart());
    const data = {
      numberofAuthor: book.numberofAuthor,
      title: book.title,
      bookType: book.bookType,
      publication: book.publication,
      authorArrayData: book.authorArrayData,
      oldarrayData: book.oldarrayData,
      subcategory: book.subcategory,
      category: book.category,
      createdBy: userId,
      authorChange: book.authorChange,
      edition: book.edition,
      isbnNumber: book.isbnNumber,
      language: book.language,
      numberOfPage: book.numberOfPage,
      publishedDate: book.publishedDate,
      authorType: book.authorType
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .put(mainLink + `/api/v1/books/${id}/`, data)
      .then(res => {
        console.log(res.data)
        dispatch(bookEditSuccess(res.data));
        dispatch(getBook(token))


      }
      ).catch(err => {
        dispatch(bookEditFail(err));

      });
  };
};


export const getDetailBook = (token, id) => {
  return (dispatch) => {
    dispatch(bookEditStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + `/api/v1/books/${id}/`)
      .then((res) => {
        dispatch(bookEditSuccess(res.data));
        console.log(res.data)
      })
      .catch((err) => {
        dispatch(bookEditFail(err));
      });
  };
};

export const deleteBook = (bookId, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .delete(mainLink + `/api/v1/books/${bookId}/`)
      .then(res => {
        dispatch(getBook(token))
        dispatch(bookPostSuccess(bookId));

        console.log(res)
      }
      )
      .catch(err => {
        dispatch(bookPostFail(err));
        console.log(err)
      });
  };
};










export const postBookEntry = (bookEntry, token, userId, printsetting, onSuccess = null, onError = null) => {

  console.log(printsetting)
  return dispatch => {
    dispatch(bookEntryPostStart());
    const data = {
      numberofAuthor: bookEntry.numberofAuthor,
      bookType: bookEntry.bookType,
      authorArrayData: bookEntry.authorArrayData,
      bookExists: bookEntry.bookExists,
      existingbookId: bookEntry.existingbookId,
      publication: bookEntry.publication,
      authorType: bookEntry.authorType,
      authorChange: bookEntry.authorChange,
      title: bookEntry.title,
      author: bookEntry.author,
      subcategory: bookEntry.subcategory,
      category: bookEntry.category,
      edition: bookEntry.edition,
      isbnNumber: bookEntry.isbnNumber,
      language: bookEntry.language,
      numberOfPage: bookEntry.numberOfPage,
      publishedDate: bookEntry.publishedDateExact,
      new: bookEntry.new,
      userId: userId,
      book: bookEntry.book,
      numberOfbook: bookEntry.numberOfbook,
      rack_number: bookEntry.rack_number,
      room_number: bookEntry.room_number,
      startlibrarycode: bookEntry.startlibrarycode,
      endlibrarycode: bookEntry.endlibrarycode,
      lending_referencing: bookEntry.lending_referencing,
      ordered: bookEntry.ordered,
      librarycode: bookEntry.librarycode,
      librarycodeDataArray: bookEntry.librarycodeDataArray,
    }
    if ('batch_number' in bookEntry) {
      data['batch'] = bookEntry.batch_number
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/bookentry/", data)
      .then(res => {
        let bookEntry = res.data
        axios.get(mainLink + "/api/v1/bookentry/qrcode/?entries=" + JSON.stringify(res.data.entries))
          .then(res => {
            // let w = window.open()
            //
            // w.document.write(res.data)
            // w.document.write('<scr' + 'ipt type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</sc' + 'ript>')
            //
            // w.document.close() // necessary for IE >= 10
            // w.focus() //
            if (typeof onSuccess === 'function') {
              onSuccess(res.data)
            }
            dispatch(bookEntryPostSuccess(bookEntry, res.data));
            dispatch(getBook(token))
          })
          .catch(err => {
            dispatch(bookEntryPostFail(err));
            if (typeof onError === 'function') {
              onError(err)
            }
          })

      }
      )
      .catch(err => {
        dispatch(bookEntryPostFail(err));
        if (typeof onError === 'function') {
          onError(err)
        }

      });
  };
};





export const getBookDetailBookEntry = (token, bookId) => {
  return (dispatch) => {
    dispatch(getBookDetailBookEntryStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + `/api/v1/bookentry/${bookId}/`)
      .then((res) => {
        dispatch(getBookDetailBookEntrySuccess(res.data));
        console.log(res.data)
      })
      .catch((err) => {
        dispatch(getBookDetailBookEntryFail(err));
        console.log(err)
      });
  };
};

export const printBatchCodes = (batchNumber = [], token, onSuccess, onError, isEntries = false) => {

  return dispatch => {

    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };

    axios.get(mainLink + `/api/v1/bookentry/qrcode/?${isEntries ? 'entries' : 'batchNumber'}=` + JSON.stringify(batchNumber))
      .then(res => {
        // let w = window.open()
        //
        // w.document.write(res.data)
        // w.document.write('<scr' + 'ipt type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</sc' + 'ript>')
        //
        // w.document.close() // necessary for IE >= 10
        // w.focus() //
        dispatch(bookEntryPostSuccess({}, res.data));
        if (typeof onSuccess == "function") {
          onSuccess()
        }
      })
      .catch(err => {
        dispatch(bookEntryPostFail(err));
        if (typeof onError == "function") {
          onError()
        }
      })
  };
};


export const getBookEntryBatchNumberDetail = (token, batch_number) => {
  return (dispatch) => {
    dispatch(getBookEntryBatchNumberStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + `/api/v1/bookentry/books_based_on_batch/?batch=${batch_number}`)
      .then((res) => {
        dispatch(getBookEntryBatchNumberSuccess(res.data));
        console.log(res.data)
      })
      .catch((err) => {
        dispatch(getBookEntryBatchNumberFail(err));
        console.log(err)
      });
  };
};

export const getBookEntryBatches = (token, bookId = null, onSuccess, onError) => {
  return (dispatch) => {
    dispatch(getBookEntryBatchNumberStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + `/api/v1/bookentry/get_batches/${bookId ? '?bookId=' + bookId : ''}`)
      .then((res) => {
        dispatch(getBookEntryBatchNumberSuccess(res.data));
        if (typeof onSuccess == 'function') {
          onSuccess(res.data)
        }
      })
      .catch((err) => {
        dispatch(getBookEntryBatchNumberFail(err));
        console.log(err)
        if (typeof onSuccess == 'function') {
          onError(err)
        }
      });
  };
};













export const deleteBookEntry = (token, bookEntryId, bookId) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .delete(mainLink + `/api/v1/bookentry/${bookEntryId}/`)
      .then(res => {
        dispatch(bookPostSuccess(res.data));
        dispatch(getBookDetailBookEntry(token, bookId))
        dispatch(getBook(token))
        console.log(res)
      }
      )
      .catch(err => {
        dispatch(bookPostFail(err));
        console.log(err)
      });
  };
}

























export const postBookEdition = (title, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostEditionStart());
    const edition = { title };
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/edition/", { title })
      .then(res => {
        dispatch(bookPostEditionSuccess(edition));
        dispatch(getBookEdition(token))

      }
      )
      .catch(err => {
        dispatch(bookPostEditionFail(err));

      });
  };
};

export const getBookEdition = (token) => {
  return dispatch => {
    dispatch(bookGetEditionStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/edition/")
      .then(res => {
        console.log(res.data)
        dispatch(bookGetEditionSuccess(res.data));

      }
      ).catch(err => {
        dispatch(bookGetEditionFail(err));

      });
  };
};

export const getDetailBookEdition = (token, id) => {
  return (dispatch) => {
    dispatch(bookEditEditionStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + `/api/v1/edition/${id}/`)
      .then((res) => {
        dispatch(bookDetailEditionSuccess(res.data));
        console.log(res.data)
      })
      .catch((err) => {
        dispatch(bookEditEditionFail(err));
      });
  };
};


export const editBookEdition = (title, editionId, token) => {
  return dispatch => {
    dispatch(bookEditEditionStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .put(mainLink + `/api/v1/edition/${editionId}/`, { title })
      .then(res => {
        console.log(res.data)
        dispatch(bookEditEditionSuccess(res.data));
        dispatch(getBookEdition(token))


      }
      ).catch(err => {
        dispatch(bookEditEditionFail(err));

      });
  };
};

export const deleteBookEdition = (editionId, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .delete(mainLink + `/api/v1/edition/${editionId}/`)
      .then(res => {
        dispatch(bookPostSuccess(res.data));
        dispatch(getBookEdition(token))
        console.log(res)
      }
      )
      .catch(err => {
        dispatch(bookPostFail(err));
        console.log(err)
      });
  };
};

export const getSettings = (token) => {
  return dispatch => {
    dispatch(bookGetEditionStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/setting/")
      .then(res => {
        console.log(res.data)
        dispatch(bookGetEditionSuccess(res.data));

      }
      ).catch(err => {
        dispatch(bookGetEditionFail(err));

      });
  };
};


























































export const postComponent = (name, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostCategoryStart());
    const category = { name };
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/category/", category)
      .then(res => {
        dispatch(bookPostCategorySuccess(category));
        dispatch(getComponent(token));

      }
      )
      .catch(err => {
        dispatch(bookPostCategoryFail(err));

      });
  };
};

export const getComponent = (token) => {
  return dispatch => {
    dispatch(bookGetCategoryStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/category/")
      .then(res => {
        console.log(res.data)
        dispatch(bookGetCategorySuccess(res.data));

      }
      ).catch(err => {
        dispatch(bookGetCategoryFail(err));

      });
  };
};


export const editComponent = (name, categoryId, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookEditCategoryStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .put(mainLink + `/api/v1/category/${categoryId}/`, { name })
      .then(res => {
        console.log(res.data)
        // localStorage.setItem("categorys", JSON.stringify(res.data));
        dispatch(bookEditCategorySuccess(res.data));
        dispatch(getComponent(token));

      }
      ).catch(err => {
        dispatch(bookEditCategoryFail(err));

      });
  };
};
export const getDetailComponent = (token, id) => {
  return (dispatch) => {
    dispatch(bookEditCategoryStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + `/api/v1/category/${id}/`)
      .then((res) => {
        dispatch(bookEditCategorySuccess(res.data));
        console.log(res.data)
      })
      .catch((err) => {
        dispatch(bookEditCategoryFail(err));
      });
  };
};
export const deleteComponent = (categoryId, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostCategoryStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .delete(mainLink + `/api/v1/category/${categoryId}/`)
      .then(res => {
        dispatch(bookPostCategorySuccess(categoryId))
        dispatch(getComponent(token));
      }
      )
      .catch(err => {
        dispatch(bookPostCategoryFail(err))
      });
  };
};


















export const postSubComponent = (subcategory, token) => {
  console.log(token)
  return dispatch => {
    dispatch(bookPostSubCategoryStart());
    const data = {
      name: subcategory.name,
      category: subcategory.category
    };
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/subcategory/", data)
      .then(res => {
        console.log(res.data)
        dispatch(bookPostSubCategorySuccess(data));
        dispatch(getBookSubCategory(token))
      }
      )
      .catch(err => {
        dispatch(bookPostSubCategoryFail(err));

      });
  };
};

export const getBookSubCategory = (token) => {
  return dispatch => {
    dispatch(bookGetSubCategoryStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/subcategory/")
      .then(res => {
        console.log(res.data)
        // localStorage.setItem("subcategorys", JSON.stringify(res.data));
        dispatch(bookGetSubCategorySuccess(res.data));

      }
      ).catch(err => {
        dispatch(bookGetSubCategoryFail(err));

      });
  };
};


export const editBookSubCategory = (newSubCategory, subcategoryId, token) => {
  return dispatch => {
    dispatch(bookEditSubCategoryStart());
    const data = {
      name: newSubCategory.name,
      category: newSubCategory.category
    };
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .put(mainLink + `/api/v1/subcategory/${subcategoryId}/`, data)
      .then(res => {
        console.log(res.data)
        // localStorage.setItem("categorys", JSON.stringify(res.data));
        dispatch(bookEditSubCategorySuccess(res.data));
        dispatch(getBookSubCategory(token))


      }
      ).catch(err => {
        dispatch(bookEditSubCategoryFail(err));

      });
  };
};
export const getDetailBookSubCategory = (token, id) => {
  return (dispatch) => {
    dispatch(bookEditSubCategoryStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + `/api/v1/category/${id}/`)
      .then((res) => {
        dispatch(bookEditSubCategorySuccess(res.data));
        console.log(res.data)
      })
      .catch((err) => {
        dispatch(bookEditSubCategoryFail(err));
      });
  };
};
export const deleteBookSubCategory = (subcategoryId, token) => {
  return dispatch => {
    dispatch(bookPostCategoryStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .delete(mainLink + `/api/v1/subcategory/${subcategoryId}/`)
      .then(res => {
        dispatch(bookPostCategorySuccess(res.data));
        dispatch(getBookSubCategory(token))
        console.log(res)
      }
      )
      .catch(err => {
        dispatch(bookPostCategoryFail(err));

        console.log(err)
      });
  };
};




