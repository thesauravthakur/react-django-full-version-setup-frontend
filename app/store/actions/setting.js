import axios from "axios";
import * as actionTypes from "./actionTypes";
import * as auth from '../reducers/auth';
import * as book from '../reducers/book';
import { mainLink } from '../utils';
import { logout } from '../actions/auth';



export const getSettingStart = () => {
  return {
    type: actionTypes.GET_SETTING_START
  };
};

export const getSettingSuccess = setting => {
  return {
    type: actionTypes.GET_SETTING_SUCCESS,
    setting
  };
};

export const getSettingFail = error => {
  return {
    type: actionTypes.GET_SETTING_FAIL,
    error: error
  };
};






export const postSettingStart = () => {
  return {
    type: actionTypes.POST_SETTING_START
  };
};

export const postSettingSuccess = settingpost => {
  return {
    type: actionTypes.POST_SETTING_SUCCESS,
    settingpost
  };
};

export const postSettingFail = error => {
  return {
    type: actionTypes.POST_SETTING_FAIL,
    error: error
  };
};





export const getSetting = (token) => {
  return dispatch => {
    dispatch(getSettingStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/settings/")
      .then(res => {
        dispatch(getSettingSuccess(res.data));
        console.log(res)

      }
      ).catch(err => {
        dispatch(getSettingFail(err));
        if (err.message.includes('401')) {
          dispatch(logout());
        }
        dispatch(getSettingFail(err));




      });
  };
};

export const postSetting = (state, token, onSuccess = null, onError = null) => {
  console.log(token)
  return dispatch => {
    dispatch(postSettingStart());
    let data = new FormData();
    data.append('name', state.name);
    data.append('headmaster_name', state.headmaster_name)
    data.append('address', state.address);
    data.append('phone_number', state.phone_number);
    data.append('principal_signature', state.sigimage);
    data.append('logo', state.logimage);
    data.append('print_size', state.print_size);
    data.append('print_on_submit', state.print_on_submit);
    data.append('mail_email', state.mail_email);
    data.append('mail_subject', state.mail_subject);
    data.append('mail_text', state.mail_text);
    data.append('video_link', state.video_link);
    data.append('google_key', state.google_key);
    data.append('mail_password', state.mail_password);

    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/settings/", data)
      .then(res => {
        dispatch(postSettingSuccess(res.data));
        dispatch(getSetting(token));
        if (typeof onSuccess == 'function') {
          onSuccess()
        }

      }
      )
      .catch(err => {
        if (err.message.includes('401')) {
          dispatch(logout());
        }
        dispatch(postSettingFail(err));
        if (typeof onError == 'function') {
          onError()
        }

      });
  };
};