from rest_framework.routers import DefaultRouter
from users.views import UserViewSets

from settings.views import (SettingViewSets)
from pkk.views import (SettingDataViewSets, SliderViewSets)
router = DefaultRouter()

router.register(r'settings', SettingViewSets)
router.register(r'Slider', SliderViewSets)
router.register(r'settingsData', SettingDataViewSets)

urlpatterns = router.urls
