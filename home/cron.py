from django_cron import CronJobBase, Schedule
from io import StringIO
import csv
from django.core.mail import EmailMessage
from trainings.views import sendMailToClient
from sendmail.utils import send_mail_task

class MyCronJob(CronJobBase):
    RUN_EVERY_MINS = 20 # every 1min

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'sop.sendmail'    # a unique code

    def do(self):
        sendMailToClient()# do your thing here

class MyCronJobForTaskMail(CronJobBase):
    RUN_EVERY_MINS = 10 # every 1min

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'sop.sendmailapproval'    # a unique code

    def do(self):
        send_mail_task()
