'''Use this for production'''

from .base import *

DEBUG = True

ALLOWED_HOSTS += [
    '192.168.11.112', '127.0.0.1', 'localhost', 'soptest.stsolutions.com.np',
    'sop.stsolutions.com.np'
]

# '192.168.11.112',
#     '127.0.0.1',
#     'localhost',
#     'sop.stsolutions.com.np',

WSGI_APPLICATION = 'home.wsgi.prod.application'

# DATABASES = {
#      'default': {
#          'ENGINE': 'django.db.backends.mysql',
#          'NAME': 'lms',
#          'USER': 'root',
#          'PASSWORD': 'admin1234',
#          'HOST': 'localhost',
#          'PORT': '',
#      }
#  }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'test',
        'USER': 'testuser',
        'PASSWORD': 'pa$$word',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        # 'rest_framework.authentication.SessionAuthentication',
        # 'rest_framework.authentication.BasicAuthentication',
    ),
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator'
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator'
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.NumericPasswordValidator'
    },
]

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

CORS_ORIGIN_WHITELIST = ('http://localhost:8000', 'http://127.0.0.1:8000')
