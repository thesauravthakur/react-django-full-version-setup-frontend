from rest_framework import serializers
from rest_framework.authtoken.models import Token
from . import models


class SliderSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Slider
        fields = "__all__"
