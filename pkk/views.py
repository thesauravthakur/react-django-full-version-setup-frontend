from django.shortcuts import render
from rest_framework import viewsets
from settings import models as SettingModels
from django.db import transaction
from settings import serializers as SettingSerializer
from rest_framework import status
from rest_framework.response import Response
from rest_framework import decorators
from PIL import Image
from io import BytesIO
import shutil
import sys
import os
from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile
from . import serializers
from . import models
from django.core.files.base import ContentFile
import base64
import io
from PIL import Image


class SettingDataViewSets(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    serializer_class = SettingSerializer.SettingSerializer
    queryset = SettingModels.Setting.objects.all()

    @transaction.atomic
    def list(self, request, *args, **kwargs):
        data = {
            'name': 'Please change from setting',
            'address': 'Address',
            'phone_number': 'Phone number',
            'mail_email': 'info@stsolutions.com.np',
            'video_link': 'UCz_jZRh5ybx8Wa-Xlrzba5w',
            'google_key': 'AIzaSyBqdgAaUlrWDo1I5IsbW7lTJadvdHQmWrw',
        }
        if SettingModels.Setting.objects.all().exists():
            setting = SettingModels.Setting.objects.all()[0]
            data['name'] = setting.name
            data['address'] = setting.address
            data['phone_number'] = setting.phone_number
            data['mail_email'] = setting.mail_email
            data['video_link'] = setting.video_link
            data['google_key'] = setting.google_key
            return Response(data)
        return Response(data)


def base64_file(data, name=None):
    _format, _img_str = data.split(';base64,')
    _name, ext = _format.split('/')
    if not name:
        name = _name.split(":")[-1]
    return ContentFile(base64.b64decode(_img_str),
                       name='{}.{}'.format(name, ext))


class SliderViewSets(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    serializer_class = serializers.SliderSerializer
    queryset = models.Slider.objects.all()

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        responseData = {'required': '', 'success': False}
        data = request.data
        images = request.data['image']
        slider = models.Slider()
        slider.description = data['description']
        slider.created_by = data['created_by']
        slider.save()
        for key in images.keys():
            imgData = base64_file(data=images[key], name=f'{slider.id}{key}')
            image = models.Image()
            image.image = imgData
            image.slider = slider
            image.save()
            print(key)
        responseData['success'] = True
        return Response(responseData)