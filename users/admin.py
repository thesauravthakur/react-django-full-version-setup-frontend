from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import User
from django.contrib.auth.models import Group

# class UserAdmin(BaseUserAdmin):
#     #These fields are shown when adding the user
#     add_fieldsets = ((None, {
#         'fields':
#         ('username', 'email', 'password1', 'password2', 'branch', 'image')
#     })),
#     ('Permissions', {'fields': ('is_superuser', 'is_staff')})
#     #These field are shown after creating the form
#     fieldsets = ((None, {
#         'fields': ('username', 'email', 'password', 'branch', 'image')
#     })),
#     ('Permissions', {'fields': ('is_superuser', 'is_staff')}),
#     #These fields are shown in the table
#     list_display = ['username', 'email', 'branch', 'image']
#     search_fields = ('username', 'email')
#     ordering = ('username', )

admin.site.register(User)
admin.site.unregister(Group)
