from allauth.account.adapter import get_adapter
from rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"


class CustomRegisterSerializer(RegisterSerializer):
    address = serializers.CharField()
    pnumber = serializers.CharField()
    position = serializers.CharField()
    name = serializers.CharField()
    gender = serializers.CharField()

    class Meta:
        model = User
        # fields = "__all__"
        fields = (
            'password',
            'email',
            'address',
            'pnumber',
            'position',
            'gender',
            'name',
        )

    def get_cleaned_data(self):
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'password2': self.validated_data.get('password2', ''),
            'email': self.validated_data.get('email', ''),
            'address': self.validated_data.get('address', ''),
            'pnumber': self.validated_data.get('pnumber', ''),
            'position': self.validated_data.get('position', ''),
            'gender': self.validated_data.get('gender', ''),
            'name': self.validated_data.get('name', ''),
        }

    def save(self, request):
        print(request)
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        user.email = self.cleaned_data.get('email')
        user.username = self.cleaned_data.get('email').split("@")[0]
        user.address = self.cleaned_data.get('address')
        user.pnumber = self.cleaned_data.get('pnumber')
        user.password = self.cleaned_data.get('password1')
        user.position = self.cleaned_data.get('position')

        user.gender = self.cleaned_data.get('gender')
        user.first_name = self.cleaned_data.get('name')
        user.save()
        adapter.save_user(request, user, self)
        return user


class TokenSerializer(serializers.ModelSerializer):
    user_type = serializers.SerializerMethodField()

    class Meta:
        model = Token
        fields = ('key', 'user', 'user_type')

    def get_user_type(self, obj):
        serializer_data = UserSerializer(obj.user).data
        permissions = []
        # try:
        #     member = obj.user.member
        #     permissions = [
        #         p['user_permission']
        #         for p in member.user_type.memberpermissions_set.values(
        #             'user_permission')
        #     ]
        # except User.member.RelatedObjectDoesNotExist:
        #     permissions = False

        return {
            'name': obj.user.first_name + ' ' + obj.user.last_name,
            'permission': permissions
        }
